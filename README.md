# Kubernetes Workshop
Lab 03: Deploy and Upgrade a Single Application

---

## Instructions

### Ensure that your environment is clean

 - List existent deployments
```
kubectl get deployments
```

 - List existent pods
```
kubectl get pods
```

### Deploy an application using kubernetes deployments

 - Download the Deployment definition
```
wget https://gitlab.com/sela-aks-workshop/lab-03/raw/master/frontend-deployment.yaml
```

 - Inspect the Deployment definition
```
cat frontend-deployment.yaml
```

 - Create the Deployment resource
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-03/raw/master/frontend-deployment.yaml
```

 - List existent deployments
```
kubectl get deployments
```

 - List existent replicasets
```
kubectl get replicasets
```

 - List existent pods
```
kubectl get pods
```

 - Inspect pod images
```
kubectl describe pods | grep Image:
```

### Rolling a Zero-Downtime update

 - Duplicate the deployment definition file
```
cat frontend-deployment.yaml > frontend-deployment-v2.yaml
```

 - Edit the new definition (frontend-deployment-v2.yaml) by changing the container image to refer to the new image version
```
Replace: selaworkshops/sentiment-analysis-frontend:v1
With: selaworkshops/sentiment-analysis-frontend:v2
```

 - Apply your changes to start the rolling upgrade:
```
kubectl apply -f frontend-deployment-v2.yaml --record
```

 - We can check the status of the rollout using the following command:
```
kubectl rollout status deployment frontend
```

 - Inspect pod images
```
kubectl describe pods | grep Image:
```

### Rolling back to a previous state

 - Inspect the rollout history using:
```
kubectl rollout history deployment frontend
```

 - Rollback to the first revision:
```
kubectl rollout undo deployment frontend --to-revision=1
```

 - We can check the status of the rollout using the following command:
```
kubectl rollout status deployment frontend
```

 - Inspect pod images
```
kubectl describe pods | grep Image:
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```
